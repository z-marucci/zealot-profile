var gulp = require ('gulp'),
    uglify = require('gulp-uglify'),
    nunjucksRender = require('gulp-nunjucks-render'),
    sass = require('gulp-sass');
    imagemin = require('gulp-imagemin');

gulp.task('styles', function(){
    return gulp.src('sass/main.scss')
    .pipe(sass())
    .pipe(gulp.dest('css'));
});

gulp.task('nunjucks' ,function (){
    return gulp.src('pages/**/*.+(html|nunjucks)')
    .pipe(nunjucksRender({
        path: ['templates']
    }))
    .pipe(gulp.dest(''))
});

gulp.task('imgcrush', () =>
    gulp.src('img/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/img'))
);

gulp.task('watch', function (){
    gulp.watch(['sass/**/*.scss', 'pages/**/*.+(html|nunjucks)', 'templates/**/*.+(html|nunjucks)'], ['styles', 'nunjucks']);
});

gulp.task('default', ['styles', 'watch', 'imgcrush']);
