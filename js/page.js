$(function(){ 
    // This is a functions that scrolls to #{blah}link
  function goToByScroll(id){
        // Remove "link" from the ID
      id = id.replace("link", "");
        // Scroll
      $('html,body').animate({
          scrollTop: $("#"+id).offset().top},
          1400);
  }

  $("ul.footer-nav > li > a").click(function(e) { 
        // Prevent a page reload when a link is pressed
      e.preventDefault(); 
        // Call the scroll function
      goToByScroll(this.id);           
  });

  $('ul.navbar-nav > li > a').click(function(e) {
    e.preventDefault(); 
    var goId = $(this).attr('href').replace('#', '');
    console.log(goId);
    goToByScroll(goId);
  });
});